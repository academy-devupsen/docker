package main

import (
	"crypto/md5"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/kardianos/osext"
	"github.com/zcalusic/sysinfo"
	"io"
	"net/http"
	"os"
	"os/exec"
	"os/user"
)

type responce struct {
	Username string `json:"owner"`
	Path     string `json:"directory"`
	Mode     string `json:"mode"`
	Hash     string `json:"hash-summ"`
	Distr    string `json:"os"`
}

type msg string

var (
	mode = flag.String("mode", "none", "Режим работы приложения")
)

func FileMD5(path string) string {
	h := md5.New()
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	_, err = io.Copy(h, f)
	if err != nil {
		panic(err)
	}
	return fmt.Sprintf("%x", h.Sum(nil))
}

func ServeHTTP(resp http.ResponseWriter, req *http.Request) {

	m := msgHandler()

	fmt.Fprint(resp, m)
}

func msgHandler() (message string) {

	answer := responce{}

	user, _ := user.Current()
	answer.Username = user.Username

	answer.Path, _ = osext.ExecutableFolder()

	answer.Mode = *mode

	answer.Hash = FileMD5("/src/main.go")

	var si sysinfo.SysInfo

	si.GetSysInfo()

	answer.Distr = fmt.Sprintf("%s", si.OS.Name)

	answerJson, _ := json.Marshal(answer)

	message = string(answerJson)

	return
}

func main() {

	flag.Parse()

	fmt.Println()
	out, _ := exec.Command("figlet", "-k", "-f", "/src/pagga", "Academy Devupsen").Output()

	fmt.Println(string(out))

	fmt.Println("Start app...")

	fmt.Println()
	fmt.Println("https://devupsen.ru")
	fmt.Println()

	http.HandleFunc("/", ServeHTTP)
	http.ListenAndServe("0.0.0.0:9000", nil)

}
